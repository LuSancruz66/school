const express = require('express');

const app = express();

//Configuration Server
app.set('port', process.env.PORT || 3000);


//Middlewares
app.use(express.json());


//Routes
app.use(require('./routes/alumnos'));
app.use(require('./routes/profesores'));
app.use(require('./routes/administrativos'));
app.use(require('./routes/limpieza'));
app.use(require('./routes/materia'));


app.listen(app.get('port'), () =>{
    console.log('Servidor activo en puerto '+ app.get('port'));
})